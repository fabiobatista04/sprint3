package configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Configuration
@PropertySource("classpath:application.properties")
public class UrlConfiguration {

    @Value("{url.base}")
    public String urlBase;

    @Value("{url.people}")
    public String people;

    @Value("{url.planets}")
    public String planets;

    @Value("{url.films}")
    public String films;

    @Value("{url.species}")
    public String species;

    @Value("{url.vehicles}")
    public String vehicles;

    @Value("{url.starships}")
    public String starships;

    @Value("{url.format_json}")
    public String jsonFormat;

    @Value("{url.param}")
    public String search;
}
