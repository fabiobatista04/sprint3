package br.com.technocorp.service.stub;

import br.com.technocorp.dto.PersonDTO;
import br.com.technocorp.model.Person;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static java.time.LocalDate.of;

public class PersonServiceStub {

    private static final LocalDate BIRHDATE = of(1993,8,17);

    public static Person stubPerson() {
        return Person.builder()
                .id("123")
                .name("fabio")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(24)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }
    public static PersonDTO stubPersonDtoDifferent() {
        return PersonDTO.builder()
                .id("123")
                .name("Manoel")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(25)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }
    public static Person stubPersonDifferent() {
        return Person.builder()
                .id("123")
                .name("Manoel")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(25)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }

    public static PersonDTO stubPersonDto() {
        return PersonDTO.builder()
                .id("123")
                .name("fabio")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(24)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }
    public static Person stubPersonIdNull() {
        return Person.builder()
                .id(null)
                .name("fabio")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(24)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }

    public static List<Person> stubPersonList(){
        return Arrays.asList(stubPerson(),stubPerson(),stubPerson());
    }
}
