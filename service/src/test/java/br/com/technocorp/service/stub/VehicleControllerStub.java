package br.com.technocorp.service.stub;

import br.com.technocorp.model.Vehicle;

public class VehicleControllerStub {

    public static Vehicle stubVehicle() {
        return Vehicle.builder()
                .name("catot")
                .model("a")
                .manufacturer("sfjkah")
                .costCredits("sfd")
                .length("sdfs")
                .max_atmosphering_speed("sfd")
                .crew("dsf")
                .passengers("sdf")
                .cargo_capacity("sdf")
                .consumables("sdfsd")
                .vehicle_class("df")
                .created("sfdfs")
                .edited("dsfsd")
                .url("sdgfsd")
                .build();
    }
}
