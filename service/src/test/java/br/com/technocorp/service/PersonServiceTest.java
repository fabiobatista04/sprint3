package br.com.technocorp.service;

import br.com.technocorp.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static br.com.technocorp.service.stub.PersonServiceStub.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PersonServiceTest {

    @Mock
    private PersonRepository repository;

    @InjectMocks
    private PersonService personService;

    @Test
    void save() {
//        when(repository.findByCpf(stubPersonDto().getCpf()))
//                .thenReturn(null);

        when(repository.save(stubPersonIdNull()))
                .thenReturn(stubPerson());

        assertEquals(stubPerson(),personService.save(stubPersonDto()));

    }

    @Test
    void update() {
        when(repository.save(stubPersonDifferent()))
                .thenReturn(stubPersonDifferent());

        when(repository.findById(stubPerson().getId()))
                .thenReturn(java.util.Optional.ofNullable(stubPerson()));

        assertEquals(stubPersonDifferent(),personService.update(stubPerson().getId(),stubPersonDtoDifferent()));



    }

    @Test
    void findByName() {

        when(repository.findByNameIgnoreCase(stubPerson().getName()))
                .thenReturn(stubPersonList());

        assertEquals(stubPersonList(),personService.findByName(stubPerson().getName()));

    }

    @Test
    void findAll() {

        when(repository.findAll())
                .thenReturn(stubPersonList());

        assertEquals(stubPersonList(),personService.findAll());
    }

    @Test
    void findById() {
        when(repository.findById(stubPerson().getId()))
                .thenReturn(Optional.of(stubPerson()));

        assertEquals(stubPerson(),personService.findById(stubPerson().getId()));
    }

    @Test
    void findByCpf() {

        when(repository.findByCpf(stubPerson().getCpf()))
                .thenReturn(stubPerson());

        assertEquals(stubPerson(),personService.findByCpf(stubPerson().getCpf()));
    }
}