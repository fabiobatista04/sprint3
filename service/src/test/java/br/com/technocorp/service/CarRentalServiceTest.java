package br.com.technocorp.service;

import br.com.technocorp.dto.PersonDTO;
import br.com.technocorp.repository.CarRentalRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static br.com.technocorp.service.stub.CarRentalServiceStub.*;
import static br.com.technocorp.service.stub.PersonServiceStub.stubPerson;
import static br.com.technocorp.service.stub.PersonServiceStub.stubPersonDto;
import static br.com.technocorp.service.stub.VehicleControllerStub.stubVehicle;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarRentalServiceTest {
    @Mock
    private CarRentalRepository carRentalRepository;

    @Mock
    private PersonService personService;

    @Mock
    private VehicleService vehicleService;

    @InjectMocks
    private CarRentalService carRentalService;

    //    RECEIVE CarRental]
    //    RETURTN CarRental
    @Test
    void save() {

        when(personService.findByCpf(stubPerson().getCpf()))
                .thenReturn(stubPerson());

        when(vehicleService.findFirstByName(stubVehicle().getName()))
                .thenReturn(stubVehicle());

        when(carRentalRepository.save(stubCarRental()))
                    .thenReturn(stubCarRental());

        assertEquals(stubCarRental(),carRentalService.save(stubCarRental()));

    }
    void mustReturnErrorBadGatewayWhenSave() {

        when(personService.findByCpf(stubPerson().getCpf()))
                .thenReturn(stubPerson());

        when(vehicleService.findFirstByName(stubVehicle().getName()))
                .thenReturn(stubVehicle());

        when(carRentalRepository.save(stubCarRental()))
                .thenReturn(stubCarRental());

        assertEquals(stubCarRental(),carRentalService.save(stubCarRental()));

    }

    @Test
    void devolutionCarRent() {
        PersonDTO dto = stubPersonDto();

        when(carRentalRepository.findFirstByPersonCpfAndPersonIsRentedCar(dto.getCpf(),true))
                .thenReturn(stubCarRental());
        when(carRentalRepository.save(stubCarRentalDevolution()))
                .thenReturn(stubCarRentalDevolution());

        assertEquals(stubCarRentalDevolution(),carRentalService.devolutionCarRent(stubPersonDto()));
    }

    @Test
    void findAll() {
        when(carRentalRepository.findAll())
                .thenReturn(stubCarRentalList());

        assertEquals(stubCarRentalList(),carRentalService.findAll());
    }
}