package br.com.technocorp.service.exception;

public class ConnectionRefused extends RuntimeException {

     public ConnectionRefused(String msg){
        super(msg);
    }
}
