package br.com.technocorp.service;

import br.com.technocorp.model.PageableResponse;
import br.com.technocorp.model.PageableVehicle;
import br.com.technocorp.model.Vehicle;
import br.com.technocorp.service.exception.ConnectionRefused;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Service
public class VehicleService {

    private static final String URLBASE = "https://swapi.dev/api/vehicles/";
    private static final String SEARCH = "?search=";
    private static final String ANDFORMAT ="&format=json";
    private static final String JSONFORMAT = "/?format=json";

    public Vehicle findById(int id){
        return findResourceByUrl(URLBASE + id + JSONFORMAT,
                Vehicle.class);
    }

    public List<Vehicle> findByName(String name){
        return findResourceByUrl(URLBASE + SEARCH + name + ANDFORMAT,
                PageableVehicle.class)
                .getResults();
    }
    public Vehicle findFirstByName(String name){
        return findResourceByUrl(URLBASE + SEARCH+ name + ANDFORMAT,
                PageableVehicle.class)
                .getResults()
                .get(0);
    }
    public List<Vehicle> findByModel(String model){
       return findResourceByUrl(URLBASE + SEARCH+ model + ANDFORMAT,
               PageableResponse.class)
               .getResults();
    }
    public List<Vehicle> findAll(){
       return findResourceByUrl(URLBASE + JSONFORMAT,
               PageableResponse.class)
               .getResults();
    }

    public Vehicle rentVehicle(){return null;}

    private <T> T findResourceByUrl(String url, Class<T> responseType){
        T response =  new RestTemplate().getForEntity(url, responseType).getBody();
        if (Objects.isNull(response)) throw new ConnectionRefused("Não foi possivel econtrar o veiculo, tente mais tarde");
        return response;
    }
}
