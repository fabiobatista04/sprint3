package br.com.technocorp.service;

import br.com.technocorp.repository.PersonRepository;
import br.com.technocorp.dto.PersonDTO;
import br.com.technocorp.model.Person;
import br.com.technocorp.service.exception.IllegalParameterException;
import br.com.technocorp.service.exception.ObjectNotFoundException;
import br.com.technocorp.util.FormatCpf;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static br.com.technocorp.model.Person.DtoToPerson;

@Service
@AllArgsConstructor
public class PersonService implements FormatCpf {

    private PersonRepository personRepository;

    public Person save(PersonDTO dto){
        checkName(dto.getName());
        checkAge(dto.getAge());
        checkCpf(dto.getCpf());
        dto.setCpf(formatCpf(dto.getCpf()));
        checkBirthDay(dto.getBirthDate());
        checkCnh(dto.getCnh());
        checkCpfDataBase(dto.getCpf());
        checkCnhDataBase(dto.getCnh());

        return personRepository.save(DtoToPerson(dto));
    }
    public Person update(String id, PersonDTO dto){

        Person person = findById(id);

        checkName(dto.getName());
        checkAge(dto.getAge());
        checkCpf(dto.getCpf());
        dto.setCpf(formatCpf(dto.getCpf()));

        if( !person.getCpf().equals(dto.getCpf()) ) checkCpfDataBase( dto.getCpf() );

        if( !person.getCnh().equals(dto.getCnh()) ) checkCnhDataBase( dto.getCnh() );


        person = DtoToPerson(dto);
        person.setId(id);
        return personRepository.save(person);
    }
    public List<Person> findByName( String name){
        checkName(name);
        return personRepository.findByNameIgnoreCase(name);
    }
    public List<Person> findAll() {
        return personRepository.findAll();
    }

    public Person findById(String id){
        checkId(id);
        Person person = personRepository.findById(id).orElse(null );
        if (Objects.isNull(person)) throw new ObjectNotFoundException("cliente não encontrado");
        return person;
    }
    public Person findByCpf( String cpf){
        checkCpf(formatCpf(cpf));
        Person person = personRepository.findByCpf(formatCpf(cpf));
        if (Objects.isNull(person)) throw new ObjectNotFoundException("cliente não encontrado");
        return person;
    }

    public void deleteById(String id){
        personRepository.deleteById(id);
    }

    private void checkId( String id){
        if(id.isBlank() || id.isEmpty())
            throw new IllegalParameterException("Id invalid");
    }

    private void checkName( String name){
        if(name.isEmpty() || name.isBlank() )
            throw new IllegalParameterException(" Name invalid");
    }
    private void checkCpf( String cpf) {
        if (!cpf.matches("\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}|\\d{11}") ) {
            throw new IllegalParameterException("Cpf com inválido");
        }
    }
    private void checkAge( Integer age) {
        if (age < 0 || age > 114)
            throw new IllegalParameterException("value invalid");
    }
    private void checkCpfDataBase(String cpf) {
        Person person = personRepository.findByCpf(cpf);
        if (!Objects.isNull(person))
            throw new IllegalParameterException("cpf já existe");
    }
    private void checkCnh(String cnh) {
        if (!cnh.matches("\\d{11}"))
            throw new IllegalParameterException("cnh inválida");
    }

    private void checkBirthDay(LocalDate birthDate) {
        if(birthDate.isAfter(LocalDate.now().minusYears(18)))
            throw new IllegalParameterException("Para cadastro, é necessário ter mais de 18 anos");
    }
    private void checkCnhDataBase(String cnh) {
       if (!Objects.isNull(personRepository.findByCnh(cnh)))
           throw new IllegalParameterException("Carteira de motorista já cadastrada");

    }
}
