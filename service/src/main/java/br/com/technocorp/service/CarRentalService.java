package br.com.technocorp.service;

import br.com.technocorp.dto.PersonDTO;
import br.com.technocorp.model.CarRental;
import br.com.technocorp.model.Person;
import br.com.technocorp.model.Vehicle;
import br.com.technocorp.repository.CarRentalRepository;
import br.com.technocorp.service.exception.IllegalParameterException;
import br.com.technocorp.service.exception.ObjectNotFoundException;
import br.com.technocorp.util.FormatCpf;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class CarRentalService implements FormatCpf {

    private CarRentalRepository carRentalRepository;
    private PersonService personService;
    private VehicleService vehicleService;
    private final BigDecimal rentalDay = BigDecimal.valueOf(80);

    public CarRental save (CarRental carRental) {
        checkPersonRentedCar(carRental);
        Person person = personService.findByCpf(carRental.getPerson().getCpf());
        if (Objects.isNull(person)) throw new ObjectNotFoundException("Cliente não encontrado");
        person.setIsRentedCar(true);
        Vehicle vehicle = vehicleService.findFirstByName(carRental.getVehicle().getName());
        carRental.setRentalDate(LocalDate.now());
        carRental.setDailyPrice(rentalDay);
        carRental.setPerson(person);
        carRental.setVehicle(vehicle);

        return carRentalRepository.save(carRental);
    }

    public CarRental devolutionCarRent( PersonDTO person){
        String cpf = formatCpf(person.getCpf());
        if (carRentalRepository.findFirstByPersonCpfAndPersonIsRentedCar(cpf,true) == null) throw new IllegalParameterException("Cliente não possui veiculo alugado");
        CarRental carRental = carRentalRepository.findFirstByPersonCpfAndPersonIsRentedCar(cpf,true);
        carRental.setDevolutionDate(LocalDate.now().plusDays(3));
        long differenceInDays = ChronoUnit.DAYS.between(carRental.getRentalDate(), carRental.getDevolutionDate());
        long rentalPrice = differenceInDays * carRental.getDailyPrice().longValue();
        carRental.setRentalPrice(BigDecimal.valueOf(rentalPrice));
        carRental.getPerson().setIsRentedCar(false);
        return carRentalRepository.save(carRental);


    }

    public List<CarRental> findAll() {
        return carRentalRepository.findAll();
    }

    private void checkPersonRentedCar(CarRental carRental) {
        String cpf = formatCpf(carRental.getPerson().getCpf());
        Boolean isRentedCar = true;
        CarRental carRentalReturned = carRentalRepository.findFirstByPersonCpfAndPersonIsRentedCar(cpf, isRentedCar);
        if (carRentalReturned != null) throw new IllegalParameterException("Cliente já possui veiculo alugado");
    }
}
