package br.com.technocorp.service.exception;

public class MethodNotSupportedException extends RuntimeException {
    public MethodNotSupportedException (String msg){
        super(msg);
    }
}
