package br.com.technocorp.service.exception;

public class IllegalParameterException extends RuntimeException{

    public IllegalParameterException(String msg){
        super(msg);
    }
}
