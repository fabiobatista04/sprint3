package br.com.technocorp.repository;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.test.context.junit4.SpringRunner;

import static br.com.technocorp.repository.stub.CarRentalRepositoryStub.stubCarRental;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@DataJdbcTest
class CarRentalRepositoryTest {


    private CarRentalRepository repository;

    CarRentalRepositoryTest(CarRentalRepository carRentalRepository){
        this.repository = carRentalRepository;
    }

    @Test
    void findFirstByPersonCpfAndPersonIsRentedCar() {

        assertEquals(stubCarRental(),repository.findFirstByPersonCpfAndPersonIsRentedCar(stubCarRental().getPerson().getCpf(), true));
    }
}