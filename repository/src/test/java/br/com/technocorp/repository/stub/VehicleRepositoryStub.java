package br.com.technocorp.repository.stub;

import br.com.technocorp.model.Vehicle;

public class VehicleRepositoryStub {

    public static Vehicle stubVehicle() {
        return Vehicle.builder()
                .name("Sail barge")
                .model("Modified Luxury Sail Barge")
                .manufacturer("Ubrikkian Industries Custom Vehicle Division")
                .length("30")
                .maxAtmosphereSpeed("100")
                .crew("26")
                .passengers("500")
                .cargoCapacity("2000000")
                .consumables("Live food tanks")
                .vehicleClass("sail barge")
                .created("2014-12-18T10:44:14.217000Z")
                .edited("2014-12-20T21:30:21.684000Z")
                .url("http://swapi.dev/api/vehicles/24/")
                .costCredits("http://swapi.dev/api/vehicles/24/")
                .build();
    }
}
