package br.com.technocorp.repository;

import br.com.technocorp.model.CarRental;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRentalRepository extends MongoRepository<CarRental,String> {

    CarRental findFirstByPersonCpfAndPersonIsRentedCar(String cpf, boolean isRentedCar);
}
