package br.com.technocorp.repository;


import br.com.technocorp.model.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends MongoRepository<Person, String> {
    List<Person> findByNameIgnoreCase(String name);
    Person findByCpf(String cpf);
    Person findByCnh(String cnh);
}
