package br.com.technocorp.model;

import br.com.technocorp.dto.PersonDTO;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Person {
    @Id
    private String id;
    private String name;
    private String cpf;
    private LocalDate birthDate;
    private Integer age;
    private String cnh;
    @Builder.Default
    private Boolean isRentedCar = false;

    public static Person DtoToPerson(PersonDTO dto){
        Person person = new Person();
        person.setName(dto.getName());
        person.setCpf(dto.getCpf());
        person.setBirthDate(dto.getBirthDate());
        person.setAge(dto.getAge());
        person.setCnh(dto.getCnh());
        person.setIsRentedCar(dto.getIsRentedCar());
        return person;
    }
}
