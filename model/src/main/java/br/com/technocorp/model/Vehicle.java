package br.com.technocorp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Vehicle {
    
    private String name;
    private String model;
    private String manufacturer;
    @JsonProperty("cost_in_credits")
    private String costCredits;
    private String length;
    @JsonProperty("max_atmosphering_speed")
    private String maxAtmosphereSpeed;
    private String crew;
    private String passengers;
    @JsonProperty("cargo_capacity")
    private String cargoCapacity;
    private String consumables;
    @JsonProperty("vehicle_class")
    private String vehicleClass;
    private Collection<String> pilots;
    private Collection<String> films;
    private String created;
    private String edited;
    private String url;
}
