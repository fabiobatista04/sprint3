package br.com.technocorp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarRental {

    @Id
    private String id;
    private Person person;
    private Vehicle vehicle;
    private BigDecimal rentalPrice;
    private LocalDate rentalDate;
    private LocalDate devolutionDate;
    private BigDecimal dailyPrice;

}
