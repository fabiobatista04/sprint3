package br.com.technocorp.model;

import lombok.Data;
import java.util.ArrayList;

@Data
public class PageableResponse<T> {
    private int count;
    private String next;
    private String previous;
    private ArrayList<T> results;
}
