package br.com.technocorp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class StarswarApplication {
	public static void main(String[] args) {
		SpringApplication.run(StarswarApplication.class, args);
	}

}
