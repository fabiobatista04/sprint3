package br.com.technocorp.core.core.stub;

import br.com.technocorp.model.Vehicle;

public class VehicleControllerStub {

    public static Vehicle stubVehicle() {
        return Vehicle.builder()
                .name("catot")
                .model("a")
                .manufacturer("sfjkah")
                .costCredits("sfd")
                .length("sdfs")
                .maxAtmosphereSpeed("sfd")
                .crew("dsf")
                .passengers("sdf")
                .cargoCapacity("sdf")
                .consumables("sdfsd")
                .vehicleClass("df")
                .created("sfdfs")
                .edited("dsfsd")
                .url("sdgfsd")
                .build();
    }
}
