package br.com.technocorp.core.core.stub;

import br.com.technocorp.dto.PersonDTO;
import br.com.technocorp.model.Person;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static java.time.LocalDate.of;

public class PersonControllerStub {

    private static final LocalDate BIRHDATE = of(1990,10,1);

    public static Person stubPerson() {
        return Person.builder()
                .id("5fda64bcfcca3d3143510b18")
                .name("Fernando")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(30)
                .cnh("12345678911")
                .isRentedCar(false)
                .build();
    }
    public static PersonDTO stubPersonDtoDifferent() {
        return PersonDTO.builder()
                .id("123")
                .name("Manoel")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(25)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }
    public static Person stubPersonDifferent() {
        return Person.builder()
                .id("123")
                .name("Manoel")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(25)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }

    public static PersonDTO stubPersonDto() {
        return PersonDTO.builder()
                .id("123")
                .name("fabio")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(24)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }
    public static Person stubPersonIdNull() {
        return Person.builder()
                .id(null)
                .name("fabio")
                .cpf("123.456.789-10")
                .birthDate(BIRHDATE)
                .age(24)
                .cnh("12345678911")
                .isRentedCar(true)
                .build();
    }

    public static List<Person> stubPersonList(){
        return Arrays.asList(stubPerson(),stubPerson(),stubPerson());
    }
}
