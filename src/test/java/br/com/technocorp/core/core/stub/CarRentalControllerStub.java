package br.com.technocorp.core.core.stub;

import br.com.technocorp.model.CarRental;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import static br.com.technocorp.core.core.stub.PersonControllerStub.stubPerson;
import static br.com.technocorp.core.core.stub.VehicleControllerStub.stubVehicle;

public class CarRentalControllerStub {
    public static CarRental stubCarRental(){
        return CarRental.builder()
                .id("123")
                .person(stubPerson())
                .vehicle(stubVehicle())
                .rentalPrice(BigDecimal.TEN)
                .rentalDate(LocalDate.now())
                .devolutionDate(LocalDate.of(2021,02,25))
                .dailyPrice(BigDecimal.valueOf(80) )
                .build();
    }
    public static CarRental stubCarRentalDifferent(){
        return CarRental.builder()
                .id("321")
                .person(stubPerson())
                .vehicle(stubVehicle())
                .rentalPrice(BigDecimal.TEN)
                .rentalDate(LocalDate.now())
                .devolutionDate(LocalDate.of(2021,03,04))
                .dailyPrice(BigDecimal.ONE)
                .build();
    }
    public static CarRental stubCarRentalDevolution(){
        CarRental carRental = stubCarRental();
        carRental.setDevolutionDate(LocalDate.now().plusDays(3));
        long differenceInDays = ChronoUnit.DAYS.between(carRental.getRentalDate(), carRental.getDevolutionDate());
        long rentalPrice = differenceInDays * carRental.getDailyPrice().longValue();
        carRental.setRentalPrice(BigDecimal.valueOf(rentalPrice));
        carRental.getPerson().setIsRentedCar(false);
        return carRental;
    }

    public static List<CarRental> stubCarRentalList(){
        return Arrays.asList(stubCarRental(),stubCarRental(),stubCarRental());
    }
}
