package br.com.technocorp.controller.exception;

import br.com.technocorp.service.exception.ConnectionRefused;
import br.com.technocorp.service.exception.IllegalParameterException;
import br.com.technocorp.service.exception.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionHandler {
    @org.springframework.web.bind.annotation.ExceptionHandler(ObjectNotFoundException.class)
    private ResponseEntity<StandardError> objectNotFoundException(ObjectNotFoundException e, HttpServletRequest request){
        HttpStatus status = HttpStatus.NOT_FOUND;
        return ResponseEntity.status(status).body(
                StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(status.value())
                        .error("Não econtrado")
                        .message(e.getMessage())
                        .path(request.getRequestURI())
                        .build()
        );
    }
    @org.springframework.web.bind.annotation.ExceptionHandler({IllegalParameterException.class})
    private ResponseEntity<StandardError> illegalParameterException(IllegalParameterException e, HttpServletRequest request){
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(
                StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(status.value())
                        .error("Parametro inválido")
                        .message(e.getMessage())
                        .path(request.getRequestURI())
                        .build()
        );
    }
    @org.springframework.web.bind.annotation.ExceptionHandler(ConnectionRefused.class)
    private ResponseEntity<StandardError> connectionRefused(ConnectionRefused e, HttpServletRequest request){
        HttpStatus status = HttpStatus.BAD_GATEWAY;
        return ResponseEntity.status(status).body(
                StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(status.value())
                        .error("Parametro inválido")
                        .error("Base de dados não está disponível")
                        .message(e.getMessage())
                        .path(request.getRequestURI())
                        .build()
        );
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    private ResponseEntity<StandardError> requestMethodNotSupported(HttpRequestMethodNotSupportedException e, HttpServletRequest request){
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(
                StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(status.value())
                        .error("Requisicão não Encontrada")
                        .error("Verifique o caminho da solicitação")
                        .message(e.getMessage())
                        .path(request.getRequestURI())
                        .build()
        );
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(HttpMessageNotReadableException.class)
    private ResponseEntity<StandardError> noContainsBody(HttpMessageNotReadableException e, HttpServletRequest request){
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(
                StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(status.value())
                        .error("Solicitação sem corpo para")
                        .error("insira os parametros para requisicao")
                        .message("insira os parametros para requisicao")
                        .path(request.getRequestURI())
                        .build()
        );
    }
}
