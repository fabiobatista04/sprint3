package br.com.technocorp.controller;

import br.com.technocorp.dto.PersonDTO;
import br.com.technocorp.model.Person;
import br.com.technocorp.service.PersonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@AllArgsConstructor
@RequestMapping("/pessoas")
@Api(value = "API REST Pessoas")
@CrossOrigin(origins = "*")
public class PersonController {

   private PersonService personService;

    @PostMapping
    @ResponseStatus(CREATED)
    @ApiOperation("Salva uma Pessoa")
    public Person save(@RequestBody PersonDTO dto){
        return personService.save(dto);
    }

    @ResponseStatus(OK)
    @PutMapping(value = "/{id}")
    @ApiOperation("Atualiza uma Pessoa")
    public Person update(@PathVariable String id, @RequestBody PersonDTO dto ){
        return personService.update(id,dto);
    }

    @ResponseStatus(OK)
    @GetMapping(value = "/id/{id}")
    @ApiOperation("Retorna uma pessoa pelo Id")
    public Person findById(@PathVariable String id){
        return personService.findById(id);
    }

    @ResponseStatus(OK)
    @GetMapping(value = "/nome/{name}")
    @ApiOperation("Retorna uma pessoa pelo nome")
    public List<Person> findByName(@PathVariable String name){
            return personService.findByName(name);
    }

    @ResponseStatus(OK)
    @GetMapping(value = "/cpf/{cpf}")
    @ApiOperation("Retorna uma Pessoa pelo CPF")
    public Person findByCpf(@PathVariable String cpf){
        return personService.findByCpf(cpf);
    }

    @ResponseStatus(OK)
    @GetMapping("/todos")
    @ApiOperation("Retorna todas as Pessoas")
    public List<Person> findAll(){
       return personService.findAll();
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping(value = "/{id}")
    @ApiOperation("Deleta uma pessoa pelo CPF")
    public void  deleteById(@PathVariable String id){
        personService.deleteById(id);
    }
}
