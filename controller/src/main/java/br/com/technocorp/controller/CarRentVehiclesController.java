package br.com.technocorp.controller;

import br.com.technocorp.dto.PersonDTO;
import br.com.technocorp.model.CarRental;
import br.com.technocorp.service.CarRentalService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("alugueis")
public class CarRentVehiclesController {

    private CarRentalService carRentalService;

    @ResponseStatus
    @GetMapping
    public List<CarRental> findAll(){
        return carRentalService.findAll();
    }

    @ResponseStatus
    @PostMapping
    public CarRental save(@RequestBody CarRental carRental){
        return carRentalService.save(carRental);
    }

    @ResponseStatus
    @PostMapping("devolucao")
    public CarRental devolution(@RequestBody PersonDTO dto){
        return carRentalService.devolutionCarRent(dto);
    }

}
