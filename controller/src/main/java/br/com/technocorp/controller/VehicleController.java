package br.com.technocorp.controller;

import br.com.technocorp.model.Vehicle;
import br.com.technocorp.service.VehicleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/veiculos")
@AllArgsConstructor
public class VehicleController {

    private VehicleService vehicleService;

    @ResponseStatus
    @GetMapping("/id/{id}")
    public Vehicle getById(@PathVariable int id) {
        return vehicleService.findById(id);
    }

    @ResponseStatus
    @GetMapping("/nome/{name}")
    public List<Vehicle> getByName(@PathVariable String name) {
        return vehicleService.findByName(name);
    }

    @ResponseStatus
    @GetMapping("/modelo/{model}")
    public List<Vehicle> getByModel(@PathVariable String model) {
        return vehicleService.findByModel(model);
    }

    @ResponseStatus
    @GetMapping
    public List<Vehicle> getAll() {
        return vehicleService.findAll();
    }
}

