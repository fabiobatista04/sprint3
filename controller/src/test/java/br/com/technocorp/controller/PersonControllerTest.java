package br.com.technocorp.controller;

import br.com.technocorp.service.PersonService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static br.com.technocorp.controller.stub.PersonControllerStub.stubPerson;
import static br.com.technocorp.controller.stub.PersonControllerStub.stubPersonDto;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
class PersonControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonService personService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void save() throws Exception {
        when(personService.save(stubPersonDto()))
                .thenReturn(stubPerson());

        var bodyExpect = objectMapper.writeValueAsString(stubPerson());

        mockMvc.perform(
                post("/pessoas")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(bodyExpect))
                .andExpect(status().isOk());
    }

    @Test
    void update() {
    }

    @Test
    void findById() {
    }

    @Test
    void findByName() {
    }

    @Test
    void findByCpf() {
    }

    @Test
    void findAll() {
    }

    @Test
    void deleteById() {
    }
}