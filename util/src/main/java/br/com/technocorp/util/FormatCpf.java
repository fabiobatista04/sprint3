package br.com.technocorp.util;

public interface FormatCpf {
    default String formatCpf(String cpf) {
        if ( cpf.matches("\\d{11}") ){
            return cpf.replaceAll("([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})","$1\\.$2\\.$3-$4");
        }else {
            return cpf;
        }
    }
}
