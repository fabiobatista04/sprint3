package br.com.technocorp.dto;

import lombok.Data;

@Data
public class VehicleDTO {

    private String name;
    private String vehicle_class;
    private String manufacturer;
    private String cost_in_credits;
    private String passengers;
    private String cargo_capacity;



}
