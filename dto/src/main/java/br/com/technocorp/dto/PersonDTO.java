package br.com.technocorp.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonDTO {
    private String id;
    private String name;
    private String cpf;
    private LocalDate birthDate;
    private Integer age;
    private String cnh;
    @Builder.Default
    private Boolean isRentedCar = false;

}
